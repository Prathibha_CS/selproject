package com.autoBot.pages;
import org.openqa.selenium.ElementNotInteractableException;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;


public class ViewLeadPage extends Annotations{
	
	public ViewLeadPage verifyFirstName() {
		WebElement ele= locateElement("id","viewLead_firstName_sp");
		verifyExactText(ele,"Prathibha");
		return this;
		
		/*String name = ele.getText();
		if(name.equals(FirstName))
		{
			System.out.println("Create Lead is successful");
		}
	
		return this;
	}*/

	}
}
