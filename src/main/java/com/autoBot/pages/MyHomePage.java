package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations{
	
	public MyLeadsPage clickLeadsLink() {
		WebElement eleLead = locateElement("link","Leads");
		click(eleLead);
		return new MyLeadsPage();
	}

	

}
