package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations{
	public CreateLeadPage clickCreateLeadLink() {
		WebElement eleLead = locateElement("link","Create Lead");
		click(eleLead);
		return new CreateLeadPage();
	}
}
