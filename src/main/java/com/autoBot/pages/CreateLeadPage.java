package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations{
	public CreateLeadPage enterCompanyName(String CName) {
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		clearAndType(eleCompName,CName);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String FName) {
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		clearAndType(eleFirstName,FName);
		return this;
	}
	
	public CreateLeadPage enterLastName(String LName) {
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		clearAndType(eleLastName,LName);
		return this;
	}
	
	public ViewLeadPage clickCreateLeadButton() {
		WebElement eleCreateButton = locateElement("xpath","//input[@value='Create Lead']");
		click(eleCreateButton);
		return new ViewLeadPage();
	}

}
